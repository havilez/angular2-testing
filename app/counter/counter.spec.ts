 import { Counter } from "./counter";

describe('Counter', () => {
    let counter: Counter;

    beforeEach( () => {
        counter = new Counter;
    })

    it('should increase value of counter when incremented', () => {
        expect(counter.value).toEqual(0);
        counter.increment();
        expect(counter.value).toEqual(1);
    });

    it('should decrease the value of counter when decremented', () => {
        expect(counter.value).toEqual(0);
        counter.decrement();
        expect(counter.value).toEqual(-1);
    })

})